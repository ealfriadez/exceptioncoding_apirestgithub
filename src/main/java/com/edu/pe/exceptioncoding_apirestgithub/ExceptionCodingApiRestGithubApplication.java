package com.edu.pe.exceptioncoding_apirestgithub;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExceptionCodingApiRestGithubApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExceptionCodingApiRestGithubApplication.class, args);
	}

}
